---
title: "Isotopes KAU"
output: 
  bookdown::gitbook:
    self_contained: true
    split_by: none
---

```{r include=FALSE}
library(tidyverse)
library(plotly)
```

```{r include=FALSE}
# load data
load(file = './data.RData')
```
```{r include=FALSE}
# dataprep
trout_prey_msd <- isodata %>% 
  filter(organism !="Plant") %>%
  filter(river!="Hudik") %>% 
  filter(!is.na(order)) %>% 
  filter(!genus_species %in% c("Lampetra planeri", "Cottus gobio",
                               "Phoxinus phoxinus", "Lota lota",
                               "Clupea harengus", "Esox lucius",
                               "Gymnocephalus cernua", "Perca fluviatilis",
                               "Rutilus rutilus", "Thymallus thymallus")) %>% 
  group_by(river, order) %>% 
  summarise(meand13C = mean(d13C),
            sdd13C = sd(d13C),
            meand15N = mean(d15N),
            sdd15N = sd(d15N))

```
# AN river

```{r echo=FALSE}
isospace_trout <- ggplot(data = trout_prey_msd %>% filter(river=='AN'),
                           aes(x = meand13C, y = meand15N, colour = order, label = order))

p_trout <- isospace_trout + geom_point() +
  geom_pointrange(aes(ymin = meand15N-sdd15N, ymax = meand15N + sdd15N)) + 
  geom_errorbar(aes(xmin = meand13C-sdd13C, xmax = meand13C + sdd13C),position = position_dodge(width = 0.5))+
  geom_text(nudge_x = 1, nudge_y = 1, cex =3)+ theme_bw()


ggplotly(p_trout)
```

# Åtjärnsbäcken river


```{r echo=FALSE}
isospace_trout <- ggplot(data = trout_prey_msd %>% filter(river=='Åtjärnsbäcken'),
                           aes(x = meand13C, y = meand15N, colour = order, label = order))

p_trout <- isospace_trout + geom_point() +
  geom_pointrange(aes(ymin = meand15N-sdd15N, ymax = meand15N + sdd15N)) + 
  geom_errorbar(aes(xmin = meand13C-sdd13C, xmax = meand13C + sdd13C),position = position_dodge(width = 0.5))+
  geom_text(nudge_x = 1, nudge_y = 1, cex =3)+ theme_bw()


ggplotly(p_trout)
```

# Gnarpsån river

```{r echo=FALSE}
isospace_trout <- ggplot(data = trout_prey_msd %>% filter(river=='Gnarpsån'),
                           aes(x = meand13C, y = meand15N, colour = order, label = order))

p_trout <- isospace_trout + geom_point() +
  geom_pointrange(aes(ymin = meand15N-sdd15N, ymax = meand15N + sdd15N)) + 
  geom_errorbar(aes(xmin = meand13C-sdd13C, xmax = meand13C + sdd13C),position = position_dodge(width = 0.5))+
  geom_text(nudge_x = 1, nudge_y = 1, cex =3)+ theme_bw()


ggplotly(p_trout)
```
# Morrumsan river

```{r echo=FALSE}
isospace_trout <- ggplot(data = trout_prey_msd %>% filter(river=='Morrumsan'),
                           aes(x = meand13C, y = meand15N, colour = order, label = order))

p_trout <- isospace_trout + geom_point() +
  geom_pointrange(aes(ymin = meand15N-sdd15N, ymax = meand15N + sdd15N)) + 
  geom_errorbar(aes(xmin = meand13C-sdd13C, xmax = meand13C + sdd13C),position = position_dodge(width = 0.5))+
  geom_text(nudge_x = 1, nudge_y = 1, cex =3)+ theme_bw()


ggplotly(p_trout)
```

# Nianån river

```{r echo=FALSE}
isospace_trout <- ggplot(data = trout_prey_msd %>% filter(river=='Nianån'),
                           aes(x = meand13C, y = meand15N, colour = order, label = order))

p_trout <- isospace_trout + geom_point() +
  geom_pointrange(aes(ymin = meand15N-sdd15N, ymax = meand15N + sdd15N)) + 
  geom_errorbar(aes(xmin = meand13C-sdd13C, xmax = meand13C + sdd13C),position = position_dodge(width = 0.5))+
  geom_text(nudge_x = 1, nudge_y = 1, cex =3)+ theme_bw()


ggplotly(p_trout)
```
